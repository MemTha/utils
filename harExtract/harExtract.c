#include <stdio.h>
#include <string.h>
#include <stdlib.h>

FILE *in, *out;

void skipLine() {
  char c = 0;
  int read = fread(&c, 1, 1, in);
  while(read != 0 && c != 0x0a) {read = fread(&c, 1, 1, in);}
}

char parseChar(int l, int r) {
  if(l < 0 || r < 0) return 0;
  if(l >= '0' && l <= '9') l -= '0';
  else if(l >= 'A' && l <= 'F') l = l + 10 - 'A';
  else if(l >='a' && l <= 'f') l = l + 10 - 'a';
  else l=0;
  if(r >= '0' && r <= '9') r -= '0';
  else if(r >= 'A' && r <= 'F') r = r + 10 - 'A';
  else if(r >='a' && r <= 'f') r = r + 10 - 'a';
  else r=0;
  r = (l * 16) + r;
  if(r > 127) r -= 256;
  return (char)r;
}

void dumpField(FILE* stream) {
  char c = 0, c2 = 0;
  int read = fread(&c, 1, 1, in);
  while(c == '"' || c == ' ') {read = fread(&c, 1, 1, in);}
  while(c != '"') {
    if(c == '\\') {
      read = fread(&c, 1, 1, in);
      switch(c) {
      case 'n': c = 0x0A; break;
      case 'b': c = 0x08; break;
      case 'f': c = 0x0C; break;
      case 'r': c = 0x0D; break;
      case 't': c = 0x09; break;
      case 'u':
	fread(&c, 1, 1, in);
	fread(&c2, 1, 1, in);
	if(c != '0' || c2 != '0') {
	  c = parseChar(c, c2);
	  fwrite(&c, 1, 1, stream);
	}
	fread(&c, 1, 1, in);
	fread(&c2, 1, 1, in);
	c = parseChar(c, c2);
	break;
      }
    }
    fwrite(&c, 1, 1, stream);
    read = fread(&c, 1, 1, in);
  }
}

int scanForField(char* pat) {
  char c = 0;
  int len = strlen(pat);
  //fprintf(stderr, "scanForField (len: %d) %s\n", len, pat);
  int i = 0;
  int lineflop=0, charmatch=0, bestmatch=0;
  int read = fread(&c, 1, 1, in);
  while(read == 1) {
    while(i == 0 && c == ' ' && read == 1) read = fread(&c, 1, 1, in);
    if(pat[i] == c) {
      i++;
      //charmatch++;
      //if(i > bestmatch) bestmatch = i;
      if(i >= len) return 1;
    } else {
      i=0;
      //lineflop++;
      while(c != 0x0a && read == 1) read = fread(&c, 1, 1, in);
    }
    read = fread(&c, 1, 1, in);
  }
  //fprintf(stderr, "lineflop: %d, charmatch: %d, bestmatch: %d\n", lineflop, charmatch, bestmatch);
  return 0;
}

int main(int argc, char** argv) {
  if(argc == 1 || argc == 3 || argc > 4 || !strcmp(argv[1], "--help")) {
    fprintf(stderr, "Usage: {archive} [url outfile]\n\tone arg to list contents, three args to extract url to outfile\n");
    return 1;
  } else if(argc == 2) {
    in = fopen(argv[1], "rb");
    fprintf(stdout, "Contents:\n");
    while(scanForField("\"url\": \"")) {
      dumpField(stdout);
      putc('\n', stdout);
    }
    fclose(in);
  } else {
    in = fopen(argv[1], "rb");
    out = fopen(argv[3], "wb");
    char* pat = (char*)malloc(strlen(argv[2])+10);
    if(pat == 0) {
      fprintf(stderr, "Failed to allocate space for pattern buffer\n");
      return 2;
    }
    sprintf(pat, "\"url\": \"%s\"", argv[2]);
    if(scanForField(pat)) {
      if(!scanForField("\"text\": \"")) {
	fprintf(stderr, "Malformed har file: matching url has no text content\n");
	return 3;
      }
      dumpField(out);
    } else {
      fprintf(stderr, "Not found\n");
      return 4;
    }
    fclose(in);
    fclose(out);
  }
}
