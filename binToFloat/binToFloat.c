#include <stdio.h>
#include <stdint.h>

#define SINGLE_SIZE sizeof(float)
union SINGLE {
  float f;
  uint8_t b[SINGLE_SIZE];
};

#define DOUBLE_SIZE sizeof(double)
union DOUBLE {
  double f;
  uint8_t b[DOUBLE_SIZE];
};

#define LDOUBLE_SIZE sizeof(long double)
union DECIMAL {
  long double f;
  uint8_t b[LDOUBLE_SIZE];
};//NOT IEEE, x86 arch native, uncommon

int main(int argc, char** argv) {
  if(argc <= 1 || argv[1][0] == '-') {
    fprintf(stderr, "Usage: binToFloat {S|D|L} [0x|0xhexval|binval]\nFirst arg is bittedness: S for single (32-bit), D for double (64-bit), L for 'long double' (80-bit: uncommon x86-native non-iEEE)\nSecond argument is optional value to convert. May be prefaced with '0x' to signify that it is hex-encoded (otherwise raw data is assumed). May be omitted or the literal string '0x', in which case data to convert is read from stdin (in hex format if the second arg is '0x', otherwise raw (recommended)).\nOutput is one line per bittedness worth of input bits.\nIncomplete/extraneous input is silently ignored. If the input is signified as hex encoded, non-hex characters are silently ignored. \nReturn code is 0 unless something bizzare goes wrong.");
    return 0;
  }
  int bytedness;
  switch(argv[1][0]) {
  case 'S':
    bytedness = SINGLE_SIZE;
    break;
  case 'D':
    bytedness = DOUBLE_SIZE;
    break;
  case 'L':
    bytedness = LDOUBLE_SIZE;
    break;
  default:
    fprintf(stderr, "Illegal bittedness: %c", argv[1][0]);
    return 0;
  }
  uint8_t stdIn = argc == 2 || (argc == 3 && argv[2][0] == '0' && argv[2][1] == 'x' && argv[2][2] == 0);
  uint8_t hexmode = argc >= 3 && argv[2][0] == '0' && argv[2][1] == 'x';
  int ai = 2, ci = hexmode ? 2 : 0;
  char bytebuffer, hxbuffer[2], byteIdx;
  int stdinBuf;
  union SINGLE sBuffer;
  union DOUBLE dBuffer;
  union DECIMAL lBuffer;
  while(1) {
    if(stdIn) {
      stdinBuf = getc(stdin);
      if(stdinBuf < 0) return 0;
      bytebuffer = (char)stdinBuf;
    } else {
      while(argv[ai][ci] == 0) {
	ci = 0;
	ai++;
	if(ai >= argc) return 0;
      }
      bytebuffer = argv[ai][ci];
      ci++;
    }
    //fprintf(stderr, "Read char: %c\n", bytebuffer);
    if(hexmode) {
      if(!((bytebuffer >= '0' && bytebuffer <= '9') || (bytebuffer >= 'a' && bytebuffer <= 'f') || (bytebuffer >= 'A' && bytebuffer <= 'F'))) continue;
      hxbuffer[hexmode-1] = bytebuffer >= '0' && bytebuffer <= '9' ? bytebuffer - '0' : bytebuffer >= 'a' && bytebuffer <= 'f' ? bytebuffer - 'a' + 10 : bytebuffer - 'A' + 10;
      if(hexmode == 1) {
	hexmode = 2;
	continue;
      } else {
	bytebuffer = hxbuffer[0] * 0x10 + hxbuffer[1];
	hexmode = 1;
      }
    }
    switch(bytedness) {
    case 4:
      sBuffer.b[byteIdx] = bytebuffer;
      if(byteIdx == bytedness - 1) {
	printf("%f\n", sBuffer.f);
      }
      break;
    case 8:
      dBuffer.b[byteIdx] = bytebuffer;
      if(byteIdx == bytedness - 1)
	printf("%f\n", dBuffer.f);
      break;
    case 10:
      lBuffer.b[byteIdx] = bytebuffer;
      if(byteIdx == bytedness - 1)
	printf("%f\n", lBuffer.f);
      break;
    }
    byteIdx++;
    if(byteIdx == bytedness) byteIdx = 0;
  }
}
