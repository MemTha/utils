#include <stdio.h>

int main(int argc, char** argv) {
  long int count[256];
  int read;
  if(argc > 1) {
    fprintf(stderr, "Usage: %s <somefile\nReads the entirety of stdin and prints how often each character occurred.\n", argv[0]);
  }
  read = fgetc(stdin);
  for(read = 0;read < 256;read++) count[read] = 0;
  while(read != EOF) {
    count[read]++;
    read = fgetc(stdin);
  }
  for(read = 0;read < 256;read++) {
    printf("%02x (%c): %ld%c", read, (read > 0x1F && read < 0x7F) ? (unsigned char)read : '.', count[read], (read+1) & 7 ? '\t' : '\n');
  }
  printf("\n");
}
