#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#define BLOCKSIZE 4096

int main(int argc, char** argv) {
  if(argc < 2 || !strcmp(argv[1], "--help")) {
    fprintf(stderr, "Usage: binarysearch {pattern} {file} [file2 [file3 ...]]\n");
    return 5;
  }
  FILE* target;
  char* pattern = argv[1];
  char* filename;
  int firstRepeat;
  struct stat tstat;
  int matches = 0;
  int pattern_len = strlen(pattern);
  char block[BLOCKSIZE];
  int blockSize;
  int blockIndex, patternIndex, blockStart;
  for(firstRepeat=1;firstRepeat < pattern_len && pattern[firstRepeat] != pattern[0];firstRepeat++) {}
  //printf("firstRepeat=%d\n", firstRepeat);
  for(int i = 2;i < argc;i++) {
    blockIndex = patternIndex = blockSize = blockStart = 0;
    filename = argv[i];
    if(stat(filename, &tstat) != 0 || !S_ISREG(tstat.st_mode)) {
      fprintf(stderr, "Could not stat file or file is wrong type: %s\n", filename);
      return 4;
    }
    if(tstat.st_size < pattern_len) {
      continue;
    }
    target = fopen(filename, "rb");
    while(blockStart + blockIndex < tstat.st_size) {
      if(blockIndex >= blockSize) {
	blockIndex -= blockSize;
	blockStart += blockSize;
	blockSize = fread(block, 1, BLOCKSIZE, target);
	if(blockSize <= 0) {
	  //fprintf(stderr, "Failed to read file %s\n", filename);
	  //fclose(target);
	  //return 2;
	  goto nextfile;
	}
      } else if(blockIndex < 0) {
	if(fseek(target, blockIndex, SEEK_CUR) != 0) {
	  fprintf(stderr, "Seek failed on file %s. Fatal.\n", filename);
	}
	blockSize = fread(block, 1, BLOCKSIZE, target);
	blockStart += blockIndex;
	blockIndex = 0;
	if(blockSize <= 0) {
	  fprintf(stderr, "Failed to read file %s\n", filename);
	  fclose(target);
	  return 3;
	}
      }
      if(block[blockIndex] == pattern[patternIndex]) {
	patternIndex++;
	if(patternIndex >= pattern_len) {
	  matches++;
	  printf("File:%s: matches at offset:%d:\n", filename, 2 + blockStart + blockIndex - pattern_len);
	  //goto nextfile;
	  patternIndex = 0;
	}
      } else {
	if(patternIndex >= firstRepeat) {
	  blockIndex -= patternIndex - firstRepeat + 1;
	  //printf("Reverse::  file=%s  blockIndex=%d patternIndex=%d (now 0)\n", filename, blockIndex, patternIndex);
	}
	patternIndex = 0;
	if(blockIndex + blockStart + pattern_len > tstat.st_size) {
	  goto nextfile;
	}
      }
      blockIndex++;
    }
  nextfile:
    fclose(target);
  }
  return !matches;
}
