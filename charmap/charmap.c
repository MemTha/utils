#include <stdio.h>

#define parseHalfChar(read) (read - (read <= '9' && read >= '0' ? '0' : read <= 'F' && read >= 'A' ? ('A'-10) : ('a' - 10)))

unsigned char parseChar(char** argv, int idx, int offset) {
  return parseHalfChar(argv[idx][offset]) * 0x10 + parseHalfChar(argv[idx][offset+1]);
}

int main(int argc, char** argv) {
  unsigned char map[256];
  int i, j, k, dv, cap, read;
  if(argc == 1 || (!(argv[1][0] >= '0' && argv[1][0] <= '9') &&
		   !(argv[1][0] >= 'A' && argv[1][0] <= 'F') &&
		   !(argv[1][0] >= 'a' && argv[1][0] <= 'f'))) {
    fprintf(stderr, "Usage: %s {char | char_range}...\nReads data from stdin, replaces each character with one defined in the arguments, and outputs the result.\nCharacters are to be specified on the command line as arguments in hex. The first character specified will replace 0x00, the second will replace 0x01 etc. Individual characters must be seperated into arguments. Characters may be specified in ranges, as four hex digits, which may be in reverse order.\nExample: to replace ascii lower case characters with upper case and vice versa, but leave others unaltered, do:\n\t%s  0040 617A 5B60 415A 7BFF\nTest (in bash):\n\tlet i=$((0x30));while [ $i -le $((0x7A)) ]; do echo -ne \"\\x$(printf \"%x\" $i)\"; let i++; done | ./charmap.exe 0040 617A 5B60 415A 7BFF; echo\n", argv[0], argv[0]);
    return 0;
  }
  for(i = 0;i < 256;i++) map[i] = 0;
  k = 1;
  for(i = 0;i < 256;i++) {
    if(argv[k][2] == 0) {
      map[i] = parseChar(argv, k, 0);
    } else {
      cap = parseChar(argv, k, 2);
      j = parseChar(argv, k, 0);
      dv = cap > j ? 1 : -1;
      for(;j != cap; j+=dv){
	map[i++] = j;
      }
      map[i] = cap;
    }
    k++;
  }
  read = fgetc(stdin);
  while(read != EOF) {
    putc(map[read], stdout);
    read = fgetc(stdin);
  }
}
