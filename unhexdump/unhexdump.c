#include <stdio.h>

int main(int argc, char** argv) {
  int last = -1;
  int read;
  if(argc > 1) {
    fprintf(stderr, "Usage: %s\nReads hex encoded data from standard in and write the decoded stream to standard out. Usually used with pipes.\n", argv[0]);
    return 0;
  }
  read = getc(stdin);
  while(read != EOF) {
    if(read <= '9' && read >= '0') {
      read -= '0';
    } else if(read <= 'f' && read >= 'a') {
      read -= 'a'-10;
    } else if(read <= 'F' && read >= 'A') {
      read -= 'A'-10;
    } else {
      goto nextchar;
    }
    if(last == -1) {
      last = read;
    } else {
      putc(last * 0x10 + read, stdout);
      last = -1;
    }
  nextchar:
    read = getc(stdin);
  }
  return 0;
}
